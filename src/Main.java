package src;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

public class Main {

    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    public static void main(String[] args) throws Exception {

        System.out.println("...started");

        Mat source = Imgcodecs.imread(Util.getSource("sheet-5.jpg"));

        Scanner scanner = new Scanner(source);
        scanner.setLogging(true);
        scanner.scan();

        Util.sout("...finished");
    }
}
