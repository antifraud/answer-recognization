package src;

import java.util.stream.Collectors;
import org.opencv.core.*;

import java.util.*;
import org.opencv.imgproc.Imgproc;

import static org.opencv.core.CvType.CV_8UC1;
import static org.opencv.imgproc.Imgproc.*;
import static org.opencv.imgproc.Imgproc.drawContours;

public class Scanner {

  private static final double BLUR_KERNEL_SIZE = 3.;
  private static final int BUBBLE_SIZE = 21;
  private static final int BUBBLE_VARIANCE = 3;
  private static final int PADDING_X = 3;
  private static final int PADDING_Y = 4;
  private static final int ZERO_POINT_ID = 360;
  private static final int ZERO_POINT_ID_VARIANCE = 30;

  private static final int ZERO_POINT_ANSWER = 160;
  private static final int ZERO_POINT_ANSWER_VARIANCE = 40;

  private final Mat source;

  private final String[] options = new String[]{"A", "B", "C", "D"};

  private Mat gray;
  private Mat thresh;
  private Mat blur;
  private Mat canny;

  private boolean logging = false;

  public Scanner(Mat source) {
    this.source = source;

  }


  public void scan() {
    preProcessing();

    try {
      System.out.println(findAnswer(40));
    } catch (Exception e) {
      e.printStackTrace();
    }

    System.out.println(findStudentID());

  }

  private Rect[][] generateIdRois() {
    return generateRois(new Point(850, 210), new Point(1000, 590), new Size(28, 38),
        25, 3, 38, 4);
  }

  private Rect[][] generateAnswerRois() {
    return generateRois(new Point(69, 725), new Point(1177, 1085), new Size(210, 180),
        277, 5, 180, 10);
  }

  private Rect[][] generateRois(Point startPoint, Point endPoint, Size size, int distanceX,
      int paddingX, int distanceY, int paddingY) {
    List<Rect[]> listRois = new ArrayList<>();
    for (double y = startPoint.y; y < endPoint.y; y += distanceY) {
      List<Rect> row = new ArrayList<>();
      for (double x = startPoint.x; x < endPoint.x; x += distanceX) {
        row.add(new Rect(new Point(x + paddingX, y + paddingY),
            new Size(size.width - 2 * paddingX, size.height - 2 * paddingY)));
      }
      listRois.add(row.toArray(new Rect[0]));
    }

    return listRois.toArray(new Rect[0][]);
  }

  public void setLogging(boolean logging) {
    this.logging = logging;
  }

  private void preProcessing() {

    gray = new Mat(source.size(), CV_8UC1);
    cvtColor(source, gray, COLOR_BGR2GRAY);
    if (logging) {
      Util.write2File(gray, "gray.png");
    }

    thresh = new Mat(gray.rows(), gray.cols(), gray.type());
    threshold(gray, thresh, 200, 255, THRESH_BINARY);
    if (logging) {
      Util.write2File(thresh, "thresh.png");
    }

    blur = new Mat(gray.size(), CV_8UC1);
    blur(gray, blur, new Size(BLUR_KERNEL_SIZE, BLUR_KERNEL_SIZE));
    if (logging) {
      Util.write2File(blur, "blur.png");
    }

    canny = new Mat(gray.size(), CV_8UC1);
    Canny(blur, canny, 160, 20);
    if (logging) {
      Util.write2File(canny, "canny.png");
    }
  }

  private List<Integer> findStudentID() {

    // Get rois
    Rect[][] rois = generateIdRois();

    List<Integer> listID = new ArrayList<>();

    // Threshold of number of zero points
    int lowerBound = ZERO_POINT_ID - ZERO_POINT_ID_VARIANCE;
    int upperBound = ZERO_POINT_ID + ZERO_POINT_ID_VARIANCE;

    for (int col = 0; col < rois[0].length; col++) {
      int choiceIndex = -1;
      int choiceCount = 0; // count the number of choices, throw error if there are more than 1 choice

      for (int bubble = 0; bubble < rois.length; bubble++) {

        // Count number of non-zero points in a roi
        int nonZero = Core.countNonZero(thresh.submat(rois[bubble][col]));

        // if this bubble is a choice
        if (nonZero <= upperBound && nonZero >= lowerBound) {
          if (logging) {
            System.out.println(
                "find student id -> col " + col + " founded choice: " + bubble + " - value = "
                    + nonZero);
          }
          choiceIndex = bubble;
          choiceCount++;
        }

      }

      // if there is only 1 choice in a column, ok
      if (choiceCount == 1) {
        listID.add(choiceIndex);
      } else { // else if there is no choice or more than 1 choice -> throw exception
        throw new NumberFormatException("Invalid ID choices");
      }
    }

    return listID;
  }

  private List<MatOfPoint> findBubbles(Mat boundingImage, int bubbleSize, int variance) {

    List<MatOfPoint> contours = new ArrayList<>();
    Mat hierarchy = new Mat();
    findContours(boundingImage, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

    double minThreshold = bubbleSize - variance;
    double maxThreshold = bubbleSize + variance;

//    if (logging) {
//      sout(
//          "findBubbles > ideal circle size > minThreshold: " + minThreshold + ", maxThreshold: "
//              + maxThreshold);
//    }

    List<MatOfPoint> drafts = new ArrayList<>();
    for (MatOfPoint contour : contours) {

      Rect _rect = boundingRect(contour);
      int w = _rect.width;
      int h = _rect.height;
      double ratio = Math.max(w, h) / Math.min(w, h);

//      if (logging) {
//        sout("findBubbles > founded circle > (" + _rect.x + ", " + _rect.y + "): " + w + ":"
//            + h);
//      }

      if (ratio >= 0.9 && ratio <= 1.1) {
        if (Math.max(w, h) <= maxThreshold && Math.min(w, h) >= minThreshold) {
          drafts.add(contour);
        }
      }
    }

    Rect r = generateAnswerRois()[0][0];

    if (logging) {
      Util.write2File(thresh.submat(r), "threshsub.png");
    }

    if (logging) {
      Util.sout("findBubbles > bubbles.size: " + drafts.size());
    }

    // order bubbles on coordinate system
    Util.sortTop2Bottom(drafts);

    List<MatOfPoint> bubbles = new ArrayList<>();

    for (int j = 0; j < drafts.size(); j += options.length) {

      List<MatOfPoint> row = drafts.subList(j, j + options.length);

      Util.sortLeft2Right(row);

      bubbles.addAll(row);
    }

    return bubbles;
  }

  public List<String> findAnswer(int questionCount) throws Exception {
    Rect[][] rois = generateAnswerRois();
    List<String> res = new ArrayList<>();
    for (int col = 0; col < rois[0].length; col++) {
      for (Rect[] roi : rois) {

        // find all bubbles in roi
        List<MatOfPoint> bubblesInRoi = findBubbles(canny.submat(rois[0][0]), BUBBLE_SIZE,
            BUBBLE_VARIANCE);

        // process each 4 bubbles
        for (int answers = 0; answers < bubblesInRoi.size() / options.length; answers++) {
          int firstBubbleIndex = answers * options.length;
          List<MatOfPoint> bubblesInLine = bubblesInRoi
              .subList(firstBubbleIndex, firstBubbleIndex + options.length);

          // create bounding rectangle for each bubble
          List<Rect> bubbleRois = bubblesInLine.stream().map(Imgproc::boundingRect)
              .collect(Collectors.toList());

          // get option
          res.add(recognizeAnswer(roi[col], bubbleRois));
        }
      }
    }

    if (questionCount == res.size()) {
      return res;
    } else {
      throw new Exception("Cannot find all answers");
    }
  }

  private String recognizeAnswer(Rect roi, List<Rect> bubbles) {

    // Threshold of number of zero points
    int lowerBound = ZERO_POINT_ANSWER - ZERO_POINT_ANSWER_VARIANCE;
    int upperBound = ZERO_POINT_ANSWER + ZERO_POINT_ANSWER_VARIANCE;

    int choiceIndex = -1;
    int choiceCount = 0; // count the number of choices, throw error if there are more than 1 choice

    for (int bubble = 0; bubble < bubbles.size(); bubble++) {

      // Count number of non-zero points in a roi
      int nonZero = Core.countNonZero(thresh.submat(roi).submat(bubbles.get(bubble)));

      if (logging) {
        System.out.println(
            "recognizeAnswer -> count in bubble " + bubble + ": "
                + nonZero);
      }

      // if this bubble is a choice
      if (nonZero <= upperBound && nonZero >= lowerBound) {

        choiceIndex = bubble;
        choiceCount++;

        if (logging) {
          System.out.println(
              "recognizeAnswer -> founded choice: " + options[bubble] + " - value = "
                  + nonZero);
        }
      }
    }

    // if there is only 1 choice in a column, ok
    if (choiceCount == 1) {
      return options[choiceIndex];
    } else { // else if there is no choice or more than 1 choice -> throw exception
      if (choiceCount == 0) {
        System.out.println("recognizeAnswer -> Invalid answer: 0 choice" );
      } else {
        System.out.println("recognizeAnswer -> Invalid answer" + choiceCount + " choices");
      }
      return null;
    }
  }

  private int[] chooseFilledCircle(int[][] rows) {

    double mean = 0;
    for (int i = 0; i < rows.length; i++) {
      mean += rows[i][0];
    }
    mean = 1.0d * mean / options.length;

    int anomalouses = 0;
    for (int i = 0; i < rows.length; i++) {
      if (rows[i][0] > mean) {
        anomalouses++;
      }
    }

    if (anomalouses == options.length - 1) {

      int[] lower = null;
      for (int i = 0; i < rows.length; i++) {
        if (lower == null || lower[0] > rows[i][0]) {
          lower = rows[i];
        }
      }

      return lower;

    } else {
      return null;
    }
  }
}
